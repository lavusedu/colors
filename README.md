# Colors

Displays info about selected colors.

## Conversions
Displays different formats of the selected color. Displayed formats are:

* RGB Hex
* RGB Decimal
* RGB Percent
* CMYK
* HSL
* RGB Binary
* Name (as available at https://en.wikipedia.org/wiki/List_of_colors_(compact))

## Schemes
Displays groups of colors in common configurations. Each scheme additionally previews the positions of the colors on a WebGL hue wheel.
Displayed groups are:

* Complementary - original color and hue + 180°
* Analogous colors - hue - 30°, original, hue + 30°
* Split complementary colors - hue - 150°, original, hue + 150°
* Triadic colors - hue - 120°, original, hue + 120°
* Tetradic colors - hue - 60°, original, hue + 120°, hue + 180°

## Preview
Displays HTML element preview with selected color + inverse lightness color. Displayed previews are:

* Colored text with inverse lightness background
* Colored background with inverse lightness text
* Colored border with white background and black text
* Inverse lightness background and text, on hover animates to colored background

## History
On the left side, last 15 selected colors are displayed in a list. The History JS API is also used to store the color history. 

## Favorites
On the right side, up to 14 favorite colors are displayed in a list. The LocalStorage JS API is used to store these between page reloads. Saved entries can be removed using mouse right-click.

## Running
Depending on the browser security policy, the application can either be opened from the disk using the `file://` protocol or served over http(s).

For `http` version, run `python3 -m http.server 8080` in the project directory.

The application folder also contains a simple `server.py` script and a self-signed `localhost` certificate to enable AppCache and ServiceWorker caches in localhost. The server can be started by running `python3 server.py` in the project directory. It will respond to `https` requests on port `8080`.