"use strict"

import names_by_colors from "./names_by_colors.js"

// Contains constants, easy to manage in one place and only evaluated once
const CONST = {
	// checks that the value starts with a known prefix,
	// only contains valid characters and the rest is between 3 and 6 characters long
	RE_HEX_FORMAT: new RegExp("(:?0x|#)?([a-fA-F0-9]{3,6})"),
}

// Represents an RGB color
export default class ColorRGB {
	constructor(r, g, b) {
		this.red = Math.max(0, Math.min(r, 255))
		this.green = Math.max(0, Math.min(g, 255))
		this.blue = Math.max(0, Math.min(b, 255))
	}

	// Returns this color as ColorRGB.
	get rgb() {
		return this
	}

	// Returns [r, g, b] in range [0, 1].
	get normalized() {
		return [this.red / 255, this.green / 25, this.blue / 255]
	}

	// Returns RRGGBB string.
	get hex() {
		const r = this.red.toString(16).padStart(2, "0").toUpperCase();
		const g = this.green.toString(16).padStart(2, "0").toUpperCase();
		const b = this.blue.toString(16).padStart(2, "0").toUpperCase();

		return r + g + b
	}

	// Returns ColorHSL.
	get hsl() {
		const red = this.red / 255
		const green = this.green / 255
		const blue = this.blue / 255

		const min = Math.min(red, green, blue)
		const max = Math.max(red, green, blue)

		const chroma = max - min
		const luminance = (max + min) / 2

		let hue
		let saturation

		if (chroma == 0) {
			hue = 0
			saturation = 0
		} else {
			let segment, shift
			if (max == red) {
				segment = (green - blue) / chroma
				shift = 0
				if (segment < 0) shift = 6
			} else if (max == green) {
				segment = (blue - red) / chroma
				shift = 2
			} else {
				segment = (red - green) / chroma
				shift = 4
			}
			
			saturation = chroma / (1 - Math.abs(2 * luminance - 1))
			hue = (segment + shift) * 60
		}

		return new ColorHSL(hue, saturation, luminance)
	}

	// Returns ColorCMYK.
	get cmyk() {
		const r = this.red / 255
		const g = this.green / 255
		const b = this.blue / 255

		const key = 1 - Math.max(r, g, b)
		if (key == 1) {
			return new ColorCMYK(0, 0, 0, 1)
		} else {
			const cyan = (1 - r - key) / (1 - key)
			const magenta = (1 - g - key) / (1 - key)
			const yellow = (1 - b - key) / (1 - key)

			return new ColorCMYK(cyan, magenta, yellow, key)
		}
	}

	// Returns the known string names of this color, separated by ", ".
	get name() {
		const hex = this.hex
		const name = names_by_colors[hex]

		if (typeof name === "object" && name.length) {
			return name.join(", ")
		} else if (typeof name === "string") {
			return name
		} else {
			return ""
		}
	}

	// Returns [red, green, blue].
	get array() {
		return [this.red, this.green, this.blue]
	}

	// Returns "r, g, b".
	get string() {
		return `${this.red}, ${this.green}, ${this.blue}`
	}

	// Same as above but with percent "r%, g%, b%".
	get string_percent() {
		const red = Math.round(this.red / 0.0255) / 100
		const green = Math.round(this.green / 0.0255) / 100
		const blue = Math.round(this.blue / 0.0255) / 100
		return `${red}%, ${green}%, ${blue}%`
	}

	// Returns the inverse color.
	get inverse() {
		return new ColorRGB(255 - this.red, 255 - this.green, 255 - this.blue)
	}

	// Returns array of complementary colors with this on the left.
	get complementary() {
		const hsl = this.hsl
		const opposite = ColorRGB.convert_hsl((hsl.hue + 180) % 360, hsl.saturation, hsl.luminance)

		return [this, opposite]
	}

	// Returns array of analogous colors with this in the middle.
	get analogous() {
		const hsl = this.hsl
		const left = ColorRGB.convert_hsl((hsl.hue - 30 + 360) % 360, hsl.saturation, hsl.luminance)
		const right = ColorRGB.convert_hsl((hsl.hue + 30) % 360, hsl.saturation, hsl.luminance)

		return [left, this, right]
	}

	// Retruns array of split complementary colors with this in the middle.
	get split_complementary() {
		const hsl = this.hsl
		const left = ColorRGB.convert_hsl((hsl.hue + 180 + 30) % 360, hsl.saturation, hsl.luminance)
		const right = ColorRGB.convert_hsl((hsl.hue + 180 - 30) % 360, hsl.saturation, hsl.luminance)

		return [left, this, right]
	}

	// Returns array of triadic colors with this in the middle.
	get triadic() {
		const hsl = this.hsl
		const left = ColorRGB.convert_hsl((hsl.hue + 240) % 360, hsl.saturation, hsl.luminance)
		const right = ColorRGB.convert_hsl((hsl.hue + 120) % 360, hsl.saturation, hsl.luminance)

		return [left, this, right]
	}

	// Returns array of tetradic color with this at index 1.
	get tetradic() {
		const hsl = this.hsl
		const left = ColorRGB.convert_hsl((hsl.hue - 60 + 360) % 360, hsl.saturation, hsl.luminance)
		const rightFirst = ColorRGB.convert_hsl((hsl.hue + 120) % 360, hsl.saturation, hsl.luminance)
		const rightSecond = ColorRGB.convert_hsl((hsl.hue + 180) % 360, hsl.saturation, hsl.luminance)

		return [left, this, rightFirst, rightSecond]
	}

	// Returns white if luminance is < 0.5, otherwise returns black.
	get luminance_grey() {
		const hsl = this.hsl
		if (hsl.luminance >= 0.5) {
			return new ColorRGB(0, 0, 0)
		} else {
			return new ColorRGB(255, 255, 255)
		}
	}

	equals(other) {
		return this.red == other.red
		&& this.green == other.green
		&& this.blue == other.blue
	}

	// Inverts the color in place.
	invert() {
		this.red = 255 - this.red 
		this.green = 255 - this.green
		this.blue = 255 - this.blue
	}

	// Supported formats are 0xRGB, 0xRRGGBB, RGB, RRGGBB, #RGB, #RRGGBB.
	// Returns undefined on invalid format.
	static convert_hex(value) {
		const match = CONST.RE_HEX_FORMAT.exec(value)
		if (match === null) {
			return undefined
		}
		
		// second capture group
		const hex_string = match[2]
		let r, g, b
		if (hex_string.length == 3) {
			r = hex_string.substring(0, 1).repeat(2)
			g = hex_string.substring(1, 2).repeat(2)
			b = hex_string.substring(2, 3).repeat(2)
		} else if (hex_string.length == 6) {
			r = hex_string.substring(0, 2)
			g = hex_string.substring(2, 4)
			b = hex_string.substring(4, 6)
		} else {
			return undefined
		}

		const red = Number.parseInt(r, 16)
		if (isNaN(red)) return undefined

		const green = Number.parseInt(g, 16)
		if (isNaN(green)) return undefined

		const blue = Number.parseInt(b, 16)
		if (isNaN(blue)) return undefined

		return new ColorRGB(red, green, blue)
	}

	// https://en.wikipedia.org/wiki/HSL_and_HSV#Converting_to_RGB
	// Returns undefined on invalid input.
	static convert_hsl(hue, saturation, luminance) {
		if (
			typeof hue === "undefined" || hue < 0 || hue >= 360
			||
			typeof saturation === "undefined" || saturation < 0 || saturation > 1
			||
			typeof luminance === "undefined" || luminance < 0 || luminance > 1
		) return undefined
		
		let red, green, blue
		
		if (saturation == 0) {
			red = Math.round(luminance * 255)
			green = Math.round(luminance * 255)
			blue = Math.round(luminance * 255)
		} else {
			const chroma = (1 - Math.abs(2 * luminance - 1)) * saturation
			const x = chroma * (1 - Math.abs(hue / 60 % 2 - 1))
			const min = luminance - chroma / 2
			
			if (hue <= 60) {
				red = chroma
				green = x
				blue = 0
			} else if (hue <= 120) {
				red = x
				green = chroma
				blue = 0
			} else if (hue <= 180) {
				red = 0
				green = chroma
				blue = x
			} else if (hue <= 240) {
				red = 0
				green = x
				blue = chroma
			} else if (hue <= 300) {
				red = x
				green = 0
				blue = chroma
			} else {
				red = chroma
				green = 0
				blue = x
			}

			red = Math.round((red + min) * 255)
			green = Math.round((green + min) * 255)
			blue = Math.round((blue + min) * 255)
		}

		return new ColorRGB(red, green, blue)
	}

	// Returns undefined on invalid input.
	static convert_cmyk(cyan, magenta, yellow, key) {
		if (
			cyan < 0 || cyan > 1
			|| magenta < 0 || magenta > 1
			|| yellow < 0 || yellow > 1
			|| key < 0 || key > 1
		) return undefined
		
		const red = 255 * (1 - cyan) * (1 - key)
		const green = 255 * (1 - magenta) * (1 - key)
		const blue = 255 * (1 - yellow) * (1 - key)

		return new ColorRGB(red, green, blue)
	}

	// Returns a random rgb color.
	static random() {
		const red = Math.floor(Math.random() * 256)
		const green = Math.floor(Math.random() * 256)
		const blue = Math.floor(Math.random() * 256)

		return new ColorRGB(red, green, blue)
	}

	// Returns a random grayscale color.
	static random_gray() {
		const grey = Math.floor(Math.random() * 256)
		return new ColorRGB(grey, grey, grey)
	}
}

// Represents a CMYK color.
export class ColorCMYK {
	constructor(cyan, magenta, yellow, key) {
		this.cyan = Math.max(0, Math.min(cyan, 1))
		this.magenta = Math.max(0, Math.min(magenta, 1))
		this.yellow = Math.max(0, Math.min(yellow, 1))
		this.key = Math.max(0, Math.min(key, 1))
	}

	// Returns this color as ColorRGB.
	get rgb() {
		return ColorRGB.parse_cmyk(this.cyan, this.magenta, this.yellow, this.key)
	}

	// Returns this color as array [C, M, Y, K].
	get array() {
		return [this.cyan, this.magenta, this.yellow, this.key]
	}

	// Returns percent string "c%, m%, y%, k%".
	get string() {
		const cyan = Math.round(this.cyan * 10000) / 100
		const magenta = Math.round(this.magenta * 10000) / 100
		const yellow = Math.round(this.yellow * 10000) / 100
		const key = Math.round(this.key * 10000) / 100
		return `${cyan}%, ${magenta}, ${yellow}%, ${key}%`
	}

	equals(other) {
		return other instanceof ColorCMYK
		&& this.cyan === other.cyan
		&& this.magenta === other.magenta
		&& this.yellow === other.yellow
		&& this.key === other.key
	}
}

// Represents a HSL color.
export class ColorHSL {
	constructor(hue, saturation, luminance) {
		this.hue = Math.max(0, Math.min(hue, 360))
		this.saturation = Math.max(0, Math.min(saturation, 1))
		this.luminance = Math.max(0, Math.min(luminance, 1))
	}

	// Returns this color as ColorRGB.
	get rgb() {
		return ColorRGB.parse_hsl(this.hue, this.saturation, this.luminance)
	}

	// Returns this color as array [H, S, L].
	get array() {
		return [this.hue, this.saturation, this.luminance]
	}

	// Returns hsl string "h°, s%, l%"
	get string() {
		const hue = Math.round(this.hue * 100) / 100
		const saturation = Math.round(this.saturation * 10000) / 100
		const luminance = Math.round(this.luminance * 10000) / 100
		return `${hue}°, ${saturation}%, ${luminance}%`
	}

	equals(other) {
		return other instanceof ColorHSL
		&& this.hue === other.hue
		&& this.saturation === other.saturation
		&& this.luminance === other.luminance
	}
}

const tests = {
	assert(cond, message) {
		if (!cond) {
			const msg = message || "Assertion failed"
			if (typeof Error !== "undefined") {
				throw new Error(msg)
			}
			throw msg
		}
	},
	
	test_all() {
		console.group("Running all color tests...")
		console.time("color_test_all")

		tests.test_convert_hex()
		tests.test_convert_hsl()
		tests.test_convert_cmyk()

		tests.test_color_hex()
		tests.test_color_hsl()
		tests.test_color_cmyk()

		tests.test_complementary()
		tests.test_analogous()
		tests.test_split_complementary()
		tests.test_triadic()
		tests.test_tetradic()

		console.groupEnd()
		console.timeEnd("color_test_all")
	},
	
	test_convert_hex() {
		console.time("test_convert_hex")

		let test
		let value

		test = ColorRGB.convert_hex("0x123")
		value = new ColorRGB(1 * 16 + 1, 2 * 16 + 2, 3 * 16 + 3)
		tests.assert(test.equals(value), "0x123 conversion failed")

		test = ColorRGB.convert_hex("0x010203")
		value = new ColorRGB(1, 2, 3)
		tests.assert(test.equals(value), "0x112233 conversion failed")

		test = ColorRGB.convert_hex("#123")
		value = new ColorRGB(1 * 16 + 1, 2 * 16 + 2, 3 * 16 + 3)
		tests.assert(test.equals(value), "#123 conversion failed")

		test = ColorRGB.convert_hex("#010203")
		value = new ColorRGB(1, 2, 3)
		tests.assert(test.equals(value), "#112233 conversion failed")

		test = ColorRGB.convert_hex("123")
		value = new ColorRGB(1 * 16 + 1, 2 * 16 + 2, 3 * 16 + 3)
		tests.assert(test.equals(value), "123 conversion failed")

		test = ColorRGB.convert_hex("010203")
		value = new ColorRGB(1, 2, 3)
		tests.assert(test.equals(value), "112233 conversion failed")

		console.timeEnd("test_convert_hex")
	},

	test_convert_hsl() {
		console.time("test_convert_hsl")

		let test
		let value

		test = ColorRGB.convert_hsl(0, 1, 0.5)
		value = new ColorRGB(255, 0, 0)
		tests.assert(test.equals(value), "hsl(0, 100%, 50%) conversion failed")

		test = ColorRGB.convert_hsl(120, 1, 0.5)
		value = new ColorRGB(0, 255, 0)
		tests.assert(test.equals(value), "hsl(120, 100%, 50%) conversion failed")

		test = ColorRGB.convert_hsl(240, 1, 0.5)
		value = new ColorRGB(0, 0, 255)
		tests.assert(test.equals(value), "hsl(240, 100%, 50%) conversion failed")

		test = ColorRGB.convert_hsl(0, 0, 0.753)
		value = new ColorRGB(192, 192, 192)
		tests.assert(test.equals(value), "hsl(0, 0, 75.3%) conversion failed")

		test = ColorRGB.convert_hsl(0, 0, 0.5)
		value = new ColorRGB(128, 128, 128)
		tests.assert(test.equals(value), "hsl(0, 0, 50%) conversion failed")

		test = ColorRGB.convert_hsl(0, 0, 1)
		value = new ColorRGB(255, 255, 255)
		tests.assert(test.equals(value), "hsl(0, 0, 100%) conversion failed")

		console.timeEnd("test_convert_hsl")
	},

	test_convert_cmyk() {
		console.time("test_convert_cmyk")

		let test
		let value

		test = ColorRGB.convert_cmyk(0, 1, 1, 0)
		value = new ColorRGB(255, 0, 0)
		tests.assert(test.equals(value), "cmyk(0%, 100%, 100%, 0%) conversion failed")
		
		test = ColorRGB.convert_cmyk(1, 0, 1, 0)
		value = new ColorRGB(0, 255, 0)
		tests.assert(test.equals(value), "cmyk(100%, 0%, 100%, 0%) conversion failed")

		test = ColorRGB.convert_cmyk(1, 1, 0, 0)
		value = new ColorRGB(0, 0, 255)
		tests.assert(test.equals(value), "cmyk(100%, 100%, 0%, 0%) conversion failed")

		test = ColorRGB.convert_cmyk(0, 0, 0, 0)
		value = new ColorRGB(255, 255, 255)
		tests.assert(test.equals(value), "cmyk(0%, 0%, 0%, 0%) conversion failed")

		test = ColorRGB.convert_cmyk(0, 0, 0, 1)
		value = new ColorRGB(0, 0, 0)
		tests.assert(test.equals(value), "cmyk(0%, 0%, 0%, 100%) conversion failed")

		console.timeEnd("test_convert_cmyk")
	},


	test_color_hex() {
		console.time("test_color_hex")

		let test
		let value

		test = new ColorRGB(255, 0, 0).hex
		value = "FF0000"
		tests.assert(test === value, "rgb(255, 0, 0) HEX conversion failed")

		test = new ColorRGB(0, 255, 0).hex
		value = "00FF00"
		tests.assert(test === value, "rgb(0, 255, 0) HEX conversion failed")

		test = new ColorRGB(0, 0, 255).hex
		value = "0000FF"
		tests.assert(test === value, "rgb(0, 0, 255) HEX conversion failed")

		test = new ColorRGB(1 * 16 + 1, 2 * 16 + 2, 3 * 16 + 3).hex
		value = "112233"
		tests.assert(test === value, "rgb(17, 34, 51) HEX conversion failed")

		test = new ColorRGB(1, 2, 3).hex
		value = "010203"
		tests.assert(test === value, "rgb(1, 2, 3) HEX conversion failed")

		console.timeEnd("test_color_hex")
	},

	test_color_hsl() {
		console.time("test_color_hsl")

		let test
		let value

		test = new ColorRGB(255, 0, 0).hsl
		value = new ColorHSL(0, 1, 0.5)
		tests.assert(test.equals(value), "rgb(255, 0, 0) HSL conversion failed")

		test = new ColorRGB(0, 255, 0).hsl
		value = new ColorHSL(120, 1, 0.5)
		tests.assert(test.equals(value), "rgb(0, 255, 0) HSL conversion failed")

		test = new ColorRGB(0, 0, 255).hsl
		value = new ColorHSL(240, 1, 0.5)
		tests.assert(test.equals(value), "rgb(0, 0, 255) HSL conversion failed")

		test = new ColorRGB(255, 255, 255).hsl
		value = new ColorHSL(0, 0, 1)
		tests.assert(test.equals(value), "rgb(255, 255, 255) HSL conversion failed")

		test = new ColorRGB(0, 0, 0).hsl
		value = new ColorHSL(0, 0, 0)
		tests.assert(test.equals(value), "rgb(0, 0, 0) HSL conversion failed")

		console.timeEnd("test_color_hsl")
	},

	test_color_cmyk() {
		console.time("test_color_cmyk")

		let test
		let value

		test = new ColorRGB(255, 0, 0).cmyk
		value = new ColorCMYK(0, 1, 1, 0)
		tests.assert(test.equals(value), "rgb(255, 0, 0) CMYK conversion failed")

		test = new ColorRGB(0, 255, 0).cmyk
		value = new ColorCMYK(1, 0, 1, 0)
		tests.assert(test.equals(value), "rgb(0, 255, 0) CMYK conversion failed")

		test = new ColorRGB(0, 0, 255).cmyk
		value = new ColorCMYK(1, 1, 0, 0)
		tests.assert(test.equals(value), "rgb(0, 0, 255) CMYK conversion failed")

		test = new ColorRGB(255, 255, 255).cmyk
		value = new ColorCMYK(0, 0, 0, 0)
		tests.assert(test.equals(value), "rgb(255, 255, 255) CMYK conversion failed")

		test = new ColorRGB(0, 0, 0).cmyk
		value = new ColorCMYK(0, 0, 0, 1)
		tests.assert(test.equals(value), "rgb(0, 0, 0) CMYK conversion failed")

		console.timeEnd("test_color_cmyk")
	},

	test_complementary() {
		console.time("test_complementary")

		const test = new ColorRGB(255, 0, 0).complementary
		const value = [new ColorRGB(255, 0, 0), new ColorRGB(0, 255, 255)]
		tests.assert(
			test[0].equals(value[0]) && test[1].equals(value[1]),
			"complementary(rgb(255, 0, 0)) failed"
		)

		console.timeEnd("test_complementary")
	},

	test_analogous() {
		console.time("test_analogous")

		const test = new ColorRGB(255, 0, 0).analogous
		const value = [new ColorRGB(255, 0, 128), new ColorRGB(255, 0, 0), new ColorRGB(255, 128, 0)]
		tests.assert(
			test[0].equals(value[0]) && test[1].equals(value[1]) && test[2].equals(value[2]),
			"analogous(rgb(255, 0, 0)) failed"
		)

		console.timeEnd("test_analogous")
	},

	test_split_complementary() {
		console.time("test_split_complementary")

		const test = new ColorRGB(255, 0, 0).split_complementary
		const value = [new ColorRGB(0, 128, 255), new ColorRGB(255, 0, 0), new ColorRGB(0, 255, 128)]
		tests.assert(
			test[0].equals(value[0]) && test[1].equals(value[1]) && test[2].equals(value[2]),
			"split_complementary(rgb(255, 0, 0)) failed"
		)

		console.timeEnd("test_split_complementary")
	},

	test_triadic() {
		console.time("test_triadic")

		const test = new ColorRGB(255, 0, 0).triadic
		const value = [new ColorRGB(0, 0, 255), new ColorRGB(255, 0, 0), new ColorRGB(0, 255, 0)]
		tests.assert(
			test[0].equals(value[0]) && test[1].equals(value[1]) && test[2].equals(value[2]),
			"triadic(rgb(255, 0, 0)) failed"
		)

		console.timeEnd("test_triadic")
	},

	test_tetradic() {
		console.time("test_tetradic")

		const test = new ColorRGB(255, 0, 0).tetradic
		const value = [new ColorRGB(255, 0, 255), new ColorRGB(255, 0, 0), new ColorRGB(0, 255, 0), new ColorRGB(0, 255, 255)]
		tests.assert(
			test[0].equals(value[0]) && test[1].equals(value[1]) && test[2].equals(value[2]) && test[3].equals(value[3]),
			"tetradic(rgb(255, 0, 0)) failed"
		)

		console.timeEnd("test_tetradic")
	}
}
// tests.test_all()