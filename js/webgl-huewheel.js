"use strict"

// Handles webgl huewheel functionality.
// The huewheel and marks and drawn in fragment shader.

import Webgl from "./webgl.js"

const CONST = {
	hslMarksMax: 5,
	hslMarksKey: "hslMarks",

	quad_vertices: new Float32Array([
		-1.0, -1.0, 0.0, 1.0,
		-1.0, 1.0, 0.0, 1.0,
		1.0, -1.0, 0.0, 1.0,
		1.0, 1.0, 0.0, 1.0
	])
}
export default class WebglHuewheel extends Webgl {
	// Creates a new WebglHuewheel, takes the same parameters as the base class.
	constructor(selector, width, height, superSample) {
		super(selector, width, height, superSample)

		this.shader = prepareProgram(this.gl, VERTEX_SHADER_SOURCE, FRAGMENT_SHADER_SOURCE)
		if (this.shader == null) {
			return
		}

		this.buffers["vertices"] = prepareVerticesBuffer(this.gl,
			CONST.quad_vertices
		)

		this.gl.useProgram(this.shader.program)
		this.gl.enableVertexAttribArray(this.shader.attributeLocations["position"])

		this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.buffers["vertices"])
		this.gl.vertexAttribPointer(
			this.shader.attributeLocations["position"],
			4, // size
			this.gl.FLOAT, // type
			false, // normalize
			0, // stride, 0 = auto
			0 // offset
		)
	}

	// Sets the hsl mark under index. If hsl == null, mark is disabled.
	setHslMark(index, hsl, color, radius, radiusHollow) {
		if (typeof index !== "number" || index < 0 || index >= CONST.hslMarksMax) {
			console.error(`${index} is not a valid hslMark index.`)
			return false
		}

		if (!this.valid) {
			console.error("WebGL context not initialized.")
			return false
		}

		this.gl.uniform3f(this.shader.uniformLocations[CONST.hslMarksKey][index]["mark"], hsl[0], hsl[1], hsl[2])
		this.gl.uniform1f(this.shader.uniformLocations[CONST.hslMarksKey][index]["radius"], radius)
		this.gl.uniform3f(this.shader.uniformLocations[CONST.hslMarksKey][index]["color"], color[0], color[1], color[2])
		this.gl.uniform1f(this.shader.uniformLocations[CONST.hslMarksKey][index]["radiusHollow"], radiusHollow)
	}

	// Sets the luminance level for the wheel.
	setLuminance(luminance) {
		if (typeof luminance !== "number" || luminance < 0.0 || luminance > 1.0) {
			console.error(`${luminance} is not valid luminance value`)
			return
		}

		this.gl.uniform1f(this.shader.uniformLocations["luminance"], luminance)
	}

	// Clears the canvas and then draws the wheel.
	draw() {
		super.draw()
		this.gl.drawArrays(
			this.gl.TRIANGLE_STRIP, // type
			0, // offset
			4 // count
		)
	}
}

// Attempts to compile a shader from source.
// Returns null on error.
function compileShader(gl, type, source) {
	const shader = gl.createShader(type)
	gl.shaderSource(shader, source)
	gl.compileShader(shader)

	const success = gl.getShaderParameter(shader, gl.COMPILE_STATUS)
	if (!success) {
		console.error("Could not compile shader", gl.getShaderInfoLog(shader))
		gl.deleteShader(shader)
		return null
	}

	return shader
}
// Creates and links shader program, compiling shaders in the process.
// Returns null on error.
function createProgram(gl, vertex_source, fragment_source) {
	const vertex = compileShader(gl, gl.VERTEX_SHADER, vertex_source)
	const fragment = compileShader(gl, gl.FRAGMENT_SHADER, fragment_source)

	const program = gl.createProgram()
	gl.attachShader(program, vertex)
	gl.attachShader(program, fragment)
	gl.linkProgram(program)

	const success = gl.getProgramParameter(program, gl.LINK_STATUS)
	if (!success) {
		console.error("Could not link program", gl.getProgramInfoLog(program))
		gl.deleteProgram(program)
		return null
	}

	return program
}
// Creates program and then retrieves attribute and uniform locations.
// Returns { program, attributeLocations, uniformLocations }
function prepareProgram(gl, vertex_source, fragment_source) {
	const program = createProgram(gl, vertex_source, fragment_source)
	if (program == null) {
		return null
	}

	const attributeLocations = {}
	const uniformLocations = {}

	attributeLocations["position"] = gl.getAttribLocation(program, "position")
	
	uniformLocations["luminance"] = gl.getUniformLocation(program, "luminance")
	
	const getHslMarkLocation = function(gl, program, index) {
		return {
			mark: gl.getUniformLocation(program, `${CONST.hslMarksKey}[${index}].mark`),
			radius: gl.getUniformLocation(program, `${CONST.hslMarksKey}[${index}].radius`),
			color: gl.getUniformLocation(program, `${CONST.hslMarksKey}[${index}].color`),
			radiusHollow: gl.getUniformLocation(program, `${CONST.hslMarksKey}[${index}].radiusHollow`)
		}
	}

	uniformLocations[CONST.hslMarksKey] = new Array(CONST.hslMarksMax)
	for (let i = 0; i < CONST.hslMarksMax; i++) {
		uniformLocations[CONST.hslMarksKey][i] = getHslMarkLocation(gl, program, i)
	}

	return { program, attributeLocations, uniformLocations }
}
// Creates and prepares buffers.
function prepareVerticesBuffer(gl, vertices) {
	const bufferVertices = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, bufferVertices)
	gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW)
	return bufferVertices
}

const VERTEX_SHADER_SOURCE = `
attribute vec4 position;

varying vec2 v_position;

void main() {
	gl_Position = position;
	v_position = vec2(position.xy);
}
`
// Fragment shader source.
// Draws a huewheel at given luminance level.
// Then draws marks.
const FRAGMENT_SHADER_SOURCE = `
precision mediump float;

#define MAX_MARKS ${CONST.hslMarksMax}

struct HSLMark {
	vec3 mark;
	float radius;
	vec3 color;
	float radiusHollow;
};

uniform HSLMark ${CONST.hslMarksKey}[MAX_MARKS];
uniform float luminance;

varying vec2 v_position;

vec3 hslToRgb(const float hue, const float saturation, const float luminance) {
	if (saturation == 0.0) {
		return vec3(luminance, luminance, luminance);
	} else {
		float chroma = (1.0 - abs(2.0 * luminance - 1.0)) * saturation;
		float x = chroma * (1.0 - abs(
			mod(hue / 60.0, 2.0) - 1.0
		));
		float min = luminance - chroma / 2.0;
		
		float red = chroma;
		float green = 0.0;
		float blue = x;

		if (hue <= 60.0) {
			green = x;
			blue = 0.0;
		} else if (hue <= 120.0) {
			red = x;
			green = chroma;
			blue = 0.0;
		} else if (hue <= 180.0) {
			red = 0.0;
			green = chroma;
			blue = x;
		} else if (hue <= 240.0) {
			red = 0.0;
			green = x;
			blue = chroma;
		} else if (hue <= 300.0) {
			red = x;
			blue = chroma;
		}

		return vec3(red + min, green + min, blue + min);
	}
}

vec3 getColor(const float hue, const float saturation, const float luminance) {
	for (int i = 0; i < MAX_MARKS; i++) {
		vec2 markVec = vec2(
			cos(radians(hslMarks[i].mark.x)) * hslMarks[i].mark.y,
			sin(radians(hslMarks[i].mark.x)) * hslMarks[i].mark.y
		);
		float markDistance = distance(markVec, v_position);

		if (
			luminance == hslMarks[i].mark.z
			&& markDistance > hslMarks[i].radiusHollow
			&& markDistance < hslMarks[i].radius
		) {
			return hslMarks[i].color;
		}
	}

	return hslToRgb(hue, saturation, luminance);
}

void main() {
	float hue = degrees(atan(v_position.y, v_position.x));
	if (hue < 0.0) {
		hue += 360.0;
	}

	float saturation = length(v_position);
	if (saturation > 1.0)
		discard;

	vec3 rgb = getColor(hue, saturation, luminance);
	gl_FragColor = vec4(rgb.xyz, 1.0);
}
`