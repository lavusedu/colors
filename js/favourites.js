"use strict"

// Handles favourites listing and storage

import Color from "./color.js"

const FAVOURITES_LIMIT = 14

const favourites = {
	// Inserts a favourite into storage, limits to FAVOURITES_LIMIT entries,
	// removes existing entry if it exists before insertion.
	insert_favourite(hex) {
		const fav = favourites.get_favourites()

		const fav_index = fav.indexOf(hex)
		if (fav_index !== -1) {
			fav.splice(fav_index, 1)
		}
		fav.push(hex)

		if (fav.length > FAVOURITES_LIMIT) {
			fav.splice(0, fav.length - FAVOURITES_LIMIT)
		}

		localStorage.setItem("favourites", JSON.stringify(fav))
		favourites.fill_favourites()
	},

	// Removes a favourite from storage.
	remove_favourite(hex) {
		const fav = favourites.get_favourites()
		const fav_index = fav.indexOf(hex)
		if (fav_index !== -1) {
			fav.splice(fav_index, 1)
		}
		localStorage.setItem("favourites", JSON.stringify(fav))
		favourites.fill_favourites()
	},

	// Gets all favourites from storage (or empty array).
	get_favourites() {
		let fav = []
		try {
			fav = JSON.parse(localStorage.getItem("favourites"))
		} catch {
			return []
		}

		if (fav === null)
			return []
		return fav
	},

	// Fills element #favourites with favourites.
	fill_favourites() {
		const parent = document.querySelector("#favourites")
		const template = document.querySelector("#template_favourites-entry")

		// clear existing nodes
		let lchild = parent.lastChild
		while (lchild && lchild.nodeName.toLowerCase() === "article" && lchild.id !== "favourites-add") {
			parent.removeChild(lchild)
			lchild = parent.lastChild
		}

		const fav = favourites.get_favourites()
		for (let i = 0; i < fav.length; i++) {
			const entry = document.importNode(template.content, true).firstElementChild

			entry.style.order = fav.length - i;
			entry.style.backgroundColor = "#" + fav[i]
			entry.style.color = "#" + new Color(fav[i]).luminance_grey.hex
			entry.textContent = "#" + fav[i]

			parent.appendChild(entry)
		}
	}
}

export default favourites