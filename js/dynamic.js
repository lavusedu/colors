"use strict"

// Generates dynamic css styles from template below.

const STYLE_ID = "dynamic-style"
function generate_css(color) {
	const hex = "#" + color.hex
	
	const hsl = color.hsl
	let hex_luminance = "#" + color.luminance_grey.hex

	const css = `
		.dynamic-color {
			color: ${hex};
		}

		.dynamic-background {
			background-color: ${hex};
		}
		.dynamic-background-hover:hover {
			background-color: ${hex};
		}

		.dynamic-border {
			border-color: ${hex};
		}

		.dynamic-color-luminance {
			color: ${hex_luminance};
		}
		.dynamic-color-luminance-hover:hover {
			color: ${hex_luminance};
		}

		.dynamic-background-luminance {
			background-color: ${hex_luminance};
		}
	`

	return css
}

export default {
	update_color(color) {
		document.getElementById(STYLE_ID).textContent = generate_css(color)

		document.querySelectorAll(".dynamic-text").forEach((node) => node.textContent = "#" + color.hex)
	}
}