"use strict"

import Color from "./color.js"
import dynamic from "./dynamic.js"
import history from "./history.js"
import favourites from "./favourites.js"
import WebglHuewheel from "./webgl-huewheel.js"

// Contains constants, easy to manage in one place and only evaluated once
const CONST = {
	INPUT_SELECTOR: "#header-input input",
	INPUT_TEXT_SELECTOR: "#header-input label span",

	SUGGESTION_SELECTOR: "#header-suggestion span",
	SUGGESTION_KEY: "transitionState",
	SUGGESTION_WAIT_TIME: 5000,
	SMALL_MEDIA_QUERY: window.matchMedia("(max-width: 1023px)")
}

const STATE = {
	// This ensures that we never end up in a state where the suggestion is
	// juggling multiple timeout loops
	suggestion_timeout_id: null,

	huewheel_complementary: null,
	huewheel_analogous: null,
	huewheel_split: null,
	huewheel_triadic: null,
	huewheel_tetradic: null
}

const inner = {
	// A handler for input "input" event.
	// Also call this when we want to update the visuals but not the histories and such.
	input_input() {
		const hex_color = document.querySelector(CONST.INPUT_SELECTOR).value
		const color = Color.convert_hex(hex_color)

		const input_text = document.querySelector(CONST.INPUT_TEXT_SELECTOR)
		input_text.textContent = "#" + color.hex
		
		// update conversions article
		inner.update_conversions(color)

		// update shemes article
		inner.update_schemes(color)

		// update dynamic eyecandy
		dynamic.update_color(color)
	},

	// A handler for input "change" event.
	// Also call this when we want to update histories but not visuals.
	input_changed() {
		const hex_color = document.querySelector(CONST.INPUT_SELECTOR).value
		const color = Color.convert_hex(hex_color)
		const hex_string = color.hex

		history.insert_history(hex_string)
	},

	// Event handler for onHashChange.
	hash_changed() {
		const color = Color.convert_hex(window.location.hash)
		if (typeof color !== "undefined") {
			document.querySelector(CONST.INPUT_SELECTOR).value = window.location.hash
			inner.input_input()
		}
	},

	// Event handler for clicking on schemes.
	scheme_click(event) {
		let target = event.target
		if (target.nodeName === "li")
			target = target.querySelector("span")

		const hex_color = target.textContent
		const color = Color.convert_hex(hex_color)
		if (typeof color !== "undefined") { 
			document.querySelector(CONST.INPUT_SELECTOR).value = hex_color
			inner.input_input()
			inner.input_changed()
		}
	},

	// Event handler for clicking on history entries.
	history_click(event) {
		if (event.target.nodeName.toLowerCase() === "article") {
			const hex_color = event.target.textContent
			const color = Color.convert_hex(event.target.textContent)
			if (typeof color !== "undefined") {
				document.querySelector(CONST.INPUT_SELECTOR).value = hex_color
				inner.input_input()
				inner.input_changed()
			}
		}
	},

	// Event handler for clicking on favourites.
	favourites_click(event) {
		if (event.target.nodeName.toLowerCase() === "article") {
			console.log(event)
			if (event.target.id === "favourites-add") {
				const hex_color = document.querySelector(CONST.INPUT_SELECTOR).value
				const color = Color.convert_hex(hex_color)
				const hex_string = color.hex
				favourites.insert_favourite(hex_string)
			} else {
				const hex_color = event.target.textContent
				const color = Color.convert_hex(event.target.textContent)
				if (typeof color !== "undefined") {
					document.querySelector(CONST.INPUT_SELECTOR).value = hex_color
					inner.input_input()
					inner.input_changed()
				}
			}
		}
	},
	// Event handler for right-clicking on favourites.
	favourites_rightclick(event) {
		event.preventDefault()
		
		if (event.target.nodeName.toLowerCase() === "article") { 
			if (event.target.id !== "favourites-add") { 
				const hex_color = event.target.textContent
				favourites.remove_favourite(hex_color.substr(1))
			}
		}

		return false
	},

	// Event handler for suggestion transition.
	suggestion_transition() {
		STATE.suggestion_timeout_id = null

		const el = document.querySelector("#header-suggestion span")
		switch (el.dataset[CONST.SUGGESTION_KEY]) {
			case "fadeIn":
				el.dataset[CONST.SUGGESTION_KEY] = "wait"
				STATE.suggestion_timeout_id = setTimeout(inner.suggestion_transition, CONST.SUGGESTION_WAIT_TIME)
				break

			case "wait":
				el.dataset[CONST.SUGGESTION_KEY] = "fadeOut"
				el.style.opacity = 0
				break

			case "fadeOut":
				const color = "#" + Color.random().hex
				el.style.color = color
				el.textContent = color

				el.dataset[CONST.SUGGESTION_KEY] = "fadeIn"
				el.style.opacity = 1
				break

			default:
				el.style.opacity = 1
				el.dataset[CONST.SUGGESTION_KEY] = "wait"
				STATE.suggestion_timeout_id = setTimeout(inner.suggestion_transition, CONST.SUGGESTION_WAIT_TIME)
				break
		}
	},

	// Event handler for suggestion click.
	suggestion_click() {
		const el = document.querySelector(CONST.SUGGESTION_SELECTOR)
		document.querySelector(CONST.INPUT_SELECTOR).value = el.textContent
		// changing input.value does not invoke "input" event nor "change" event
		inner.input_input()
		inner.input_changed()
	},

	// Event handler for media query change.
	//
	// This is necessary to fix the suggestion loop because the transitionend
	// event doesn't fire when in small media layout (because of display: none).
	small_media_query_change(event) {
		if (event.matches == false) {
			if (STATE.suggestion_timeout_id == null) {
				const el = document.querySelector("#header-suggestion span")
				el.dataset[CONST.SUGGESTION_KEY] = "broken"
				inner.suggestion_transition()
			}
		}
	},

	// Updates conversions table.
	update_conversions(color) {
		function update_func(selector, conversion, code) {
			const el = document.querySelector(selector)

			if (typeof conversion === "string") {
				const el_conversion = el.querySelector(".conversion")
				if (el_conversion !== null)
					el_conversion.textContent = conversion
			}
			if (typeof code === "string") {
				const el_code = el.querySelector(".conversion-code")
				if (el_code !== null)
					el_code.textContent = code
			}
		}

		const hex_string = color.hex
		update_func("#conversion-hex", hex_string, "#" + hex_string)

		const rgb_decimal_string = color.string
		update_func("#conversion-rgb", rgb_decimal_string, `rgb(${rgb_decimal_string})`)

		const rgb_percent_string = color.string_percent
		update_func("#conversion-rgbpercent", rgb_percent_string, `rgb(${rgb_percent_string})`)

		const cmyk_string = color.cmyk.string
		update_func("#conversion-cmyk", cmyk_string)

		const hsl = color.hsl
		const hsl_string = hsl.string
		update_func("#conversion-hsl", hsl_string, `hsl(${hsl_string.replace("°", "")})`)

		const bin_string = `${color.red.toString(2).padStart(8, "0")} ${color.green.toString(2).padStart(8, "0")} ${color.blue.toString(2).padStart(8, "0")}`
		update_func("#conversion-binary", bin_string)

		update_func("#conversion-name", undefined, color.name)
	},

	// Updates schemes elements and huewheels.
	update_schemes(color) {
		function set_color_and_text(selector, hex) {
			const el = document.querySelector(selector)
			el.style.backgroundColor = hex
			el.querySelector("span").textContent = hex
		}
		function update_huewheel(wheel, colors) {
			wheel.setLuminance(colors[0].hsl.luminance)

			for (let i = 0; i < colors.length; i++) {
				const hsl = colors[i].hsl

				let hslMarkColor = [0.0, 0.0, 0.0]
				if (hsl.luminance < 0.5)
					hslMarkColor = [1.0, 1.0, 1.0]
				
				wheel.setHslMark(i, hsl.array, hslMarkColor, 0.04, 0.02)
			}
			wheel.draw()
		}

		const complementary = color.complementary
		set_color_and_text("#complementary-colors li:first-child", "#" + complementary[0].hex)
		set_color_and_text("#complementary-colors li:last-child", "#" + complementary[1].hex)
		update_huewheel(STATE.huewheel_complementary, complementary)

		const analogous = color.analogous
		set_color_and_text("#analogous-colors li:first-child", "#" + analogous[0].hex)
		set_color_and_text("#analogous-colors li:nth-child(2)", "#" + analogous[1].hex)
		set_color_and_text("#analogous-colors li:last-child", "#" + analogous[2].hex)
		update_huewheel(STATE.huewheel_analogous, analogous)

		const split = color.split_complementary
		set_color_and_text("#split-complementary-colors li:first-child", "#" + split[0].hex)
		set_color_and_text("#split-complementary-colors li:nth-child(2)", "#" + split[1].hex)
		set_color_and_text("#split-complementary-colors li:last-child", "#" + split[2].hex)
		update_huewheel(STATE.huewheel_split, split)

		const triadic = color.triadic
		set_color_and_text("#triadic-colors li:first-child", "#" + triadic[0].hex)
		set_color_and_text("#triadic-colors li:nth-child(2)", "#" + triadic[1].hex)
		set_color_and_text("#triadic-colors li:last-child", "#" + triadic[2].hex)
		update_huewheel(STATE.huewheel_triadic, triadic)

		const tetradic = color.tetradic
		set_color_and_text("#tetradic-colors li:first-child", "#" + tetradic[0].hex)
		set_color_and_text("#tetradic-colors li:nth-child(2)", "#" + tetradic[1].hex)
		set_color_and_text("#tetradic-colors li:nth-child(3)", "#" + tetradic[2].hex)
		set_color_and_text("#tetradic-colors li:last-child", "#" + tetradic[3].hex)
		update_huewheel(STATE.huewheel_tetradic, tetradic)
	},
}

// Entry function.
window.addEventListener("load", function () {
	if ("serviceWorker" in navigator) {
		navigator.serviceWorker.register("cache-worker.js")
		.then(function (reg) {
			console.log("Cache worker registered.");
		}).catch(function (error) {
			console.error("Cache worker registration failed " + error);
		});
	}
	
	// Prepare huewheel contexts.
	STATE.huewheel_complementary = new WebglHuewheel("#complementary-colors > canvas", 250, 250, 2.0)
	STATE.huewheel_analogous = new WebglHuewheel("#analogous-colors > canvas", 250, 250, 2.0)
	STATE.huewheel_split = new WebglHuewheel("#split-complementary-colors > canvas", 250, 250, 2.0)
	STATE.huewheel_triadic = new WebglHuewheel("#triadic-colors > canvas", 250, 250, 2.0)
	STATE.huewheel_tetradic = new WebglHuewheel("#tetradic-colors > canvas", 250, 250, 2.0)

	// bind and invoke input input handler
	document.querySelector(CONST.INPUT_SELECTOR).addEventListener("input", inner.input_input)
	inner.input_input()
	// bind and invoke input change handler
	document.querySelector(CONST.INPUT_SELECTOR).addEventListener("change", inner.input_changed)
	// inner.input_changed()

	// bind click handler for history
	document.querySelector("#history").addEventListener("click", inner.history_click)

	// bind click handler for favourites
	document.querySelector("#favourites").addEventListener("click", inner.favourites_click)
	document.querySelector("#favourites").addEventListener("contextmenu", inner.favourites_rightclick)

	// bind and invoke transitionend handler for suggestion box
	document.querySelector(CONST.SUGGESTION_SELECTOR).addEventListener("transitionend", inner.suggestion_transition)
	inner.suggestion_transition()
	// bind click handler for suggestion box
	document.querySelector(CONST.SUGGESTION_SELECTOR).addEventListener("click", inner.suggestion_click)
	// suggestions should reset when responsive is scaled from small to wide.
	CONST.SMALL_MEDIA_QUERY.addListener(inner.small_media_query_change);

	// bind click handler for schemes
	for (let el of document.querySelectorAll("#schemes section li")) {
		el.addEventListener("click", inner.scheme_click)
	}

	// bind hash change handler
	window.addEventListener("hashchange", inner.hash_changed)

	// fill in history and favourites.
	history.fill_history()
	favourites.fill_favourites()
})