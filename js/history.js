"use strict"

// Handles history listing and storage

import Color from "./color.js"

const HISTORY_LIMIT = 15

const history = {
	// Inserts a favourite into storage, limits to HISTORY_LIMIT entries,
	// removes existing entry if it exists before insertion.
	insert_history(hex) {
		window.history.pushState(null, "", "#" + hex)

		const his = history.get_history()

		const history_index = his.indexOf(hex)
		if (history_index !== -1) {
			his.splice(history_index, 1)
		}
		his.push(hex)

		if (his.length > HISTORY_LIMIT) {
			his.splice(0, his.length - HISTORY_LIMIT)
		}

		localStorage.setItem("history", JSON.stringify(his))
		history.fill_history()
	},

	// Gets all history entries (or empty array).
	get_history() {
		let his = []
		try {
			his = JSON.parse(localStorage.getItem("history"))
		} catch {
			return []
		}

		if (his === null)
			return []
		return his
	},

	// Fills element #history with history entries.
	fill_history() {
		const parent = document.querySelector("#history")
		const template = document.querySelector("#template_history-entry")

		// clear existing nodes
		while (parent.lastChild && parent.lastChild.nodeName.toLowerCase() === "article") {
			parent.removeChild(parent.lastChild)
		}

		const hist = history.get_history()
		for (let i = 0; i < hist.length; i++) {
			const entry = document.importNode(template.content, true).firstElementChild
			
			entry.style.order = hist.length - i;
			entry.style.backgroundColor = "#" + hist[i]
			entry.style.color = "#" + new Color(hist[i]).luminance_grey.hex
			entry.textContent = "#" + hist[i]

			parent.appendChild(entry)
		}
	}
}

export default history