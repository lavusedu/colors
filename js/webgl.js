"use strict"

// WebGL canvas wrapper class.

export default class Webgl {
	// Creates a new webgl context for element selected with selector.
	// Sets the width and height of the element.
	// The width and height of the framebuffer is scaled by superSample.
	// Returns false on error.
	constructor(selector, width, height, superSample=1.0) {		
		this.hslMarks = []
		this.width = null
		this.height = null
		this.superSample = superSample
		this.clearColor = [1.0, 1.0, 1.0, 1.0]

		this.canvas = null
		this.gl = null
		this.shader = null

		this.buffers = {}
		
		const canvas = document.querySelector(selector)
		if (canvas == null || canvas.tagName != "CANVAS") {
			console.error(`Could not find ${selector} element or it is not a canvas element.`)
			return false
		}

		const gl = canvas.getContext("webgl")
		if (gl == null) {
			console.error("Cound not initialize WebGl context.")
			return false
		}

		gl.clearColor(
			this.clearColor[0], this.clearColor[1],
			this.clearColor[2], this.clearColor[3]
		)
		gl.clear(gl.COLOR_BUFFER_BIT)

		this.canvas = canvas
		this.gl = gl

		if (this.updateSize(width, height) === false) {
			return false
		}
	}

	// Returns whether this instance is initialized.
	get valid() {
		return this.canvas != null && this.gl != null && this.shader != null
	}

	// Updates canvas and viewport size.
	updateSize(width, height) {
		if (width == null)
			width = this.width
		if (height == null)
			height = this.height
		
		if (
			typeof width !== "number" || width <= 0.0
			|| typeof height !== "number" || height <= 0.0
		) {
			console.error(`${width} and ${height} are not valid width and height value`)
			return false
		}

		this.width = width
		this.height = height

		if (this.canvas != null) {
			this.canvas.style.width = width + "px"
			this.canvas.style.height = height + "px"

			this.canvas.width = width * this.superSample
			this.canvas.height = height * this.superSample

			this.gl.viewport(0, 0, width * this.superSample, height * this.superSample)
			return true
		}
		return false
	}

	// To be overriden by a child.
	draw() {
		this.gl.clear(this.gl.COLOR_BUFFER_BIT)
	}
}