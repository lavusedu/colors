"use strict"

self.addEventListener("install", event => {
	event.waitUntil(
		caches.open("v1").then(cache => {
			return cache.addAll([
				"index.html",
				"css/index.css",
				"css/media-small.css",
				"css/media-wide.css",
				"js/color.js",
				"js/colors_by_name.js",
				"js/dynamic.js",
				"js/favourites.js",
				"js/history.js",
				"js/main.js",
				"js/names_by_colors.js",
				"js/webgl-huewheel.js",
				"js/webgl.js"
			]);
		})
	);
});

self.addEventListener("fetch", event => {
	event.respondWith(
		caches.match(event.request)
	);
});